import { ref } from 'vue'
import {api} from 'boot/axios'
import { useQuasar } from 'quasar'

export default {
    setup (){
        const $q = useQuasar()
    },
    data () {
        return {
            icon: ref(false),
            employeeid: ref(''),
            callsign: ref(''),
            nik: ref(''),
            nik_ktp: ref(''),
            fullname: ref(''),
            company: ref(''),
            regional: ref(''),
            division: ref(''),
            mainjob: ref(''),
            position: ref(''),
            joindate: ref(''),
            resigndate: ref(''),
            joincallsign: ref(''),
            releasecallsign: ref(''),
            address: ref(''),
            education: ref(''),
            level: ref(''),
            phonenum: ref(''),
            email: ref(''),
            remark: null,
            remark_resign: null,
            remark_promote: null,
            remark_movement: null,
            optionsCompany: [],
            optionsRegional: [],
            optionsDivision: [],
            optionsPosition: [],
            optionsMainjob: [],
            optionsEducation: [],
            optionsLevel: []
        }
    },
    methods:{
        getEmployeeDetails(){
            api.get('/employee/getemployeebyid', {
                headers: {
                    Authorization : localStorage.getItem('user-token')
                },
                params : {
                    'id' : this.$route.params.id
                }
            })
            .then(response => {
                if (response.data != '') {
                    this.detailsData(response.data)
                } else {
                    this.$q.notify({
                        message: 'Employee Data Not Found',
                        color: 'red',
                        icon: 'warning'
                    })
                    this.$router.push('/employee')
                }
            })
            .catch(error => {
                console.log('message : ' + error)
                this.$q.notify({
                    message: error,
                    color: 'red',
                    icon: 'warning'
                })
                this.$router.push('/employee')
            })
        },
        detailsData(response) {
            var data = response
            this.employeeid = data.employeeid
            this.callsign = data.callsign
            this.nik_ktp = data.nikktp
            this.nik = data.nik
            this.fullname = data.fullname
            this.address = data.address
            this.education = data.education
            this.phonenum = data.phonenum
            this.regional = data.regional
            this.division = data.division
            this.mainjob = data.mainJob
            this.position = data.position
            this.level = data.level
            this.joindate = data.joindate
            this.joincallsign = data.joincallsign
            this.company = data.company
            this.email = data.email
            this.remark = data.remark
            this.remark_resign = data.remark_resign
            this.remark_promote = data.remark_promote
            this.remark_movement = data.remark_movement
        },
        getPositionList(){
            api.get('/master/getallposition',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsPosition = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getMainJobList(){
            api.get('/master/getallmainjob',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsMainjob = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getDivisionList(){
            api.get('/master/getalldivision',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsDivision = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getRegionalList(){
            api.get('/master/getallregional',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsRegional = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getLevelList(){
            api.get('/master/getalllevel',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsLevel = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getEducationList(){
            api.get('/master/getalledu',{
                headers: {
                Authorization : localStorage.getItem("user-token")
                }
            })
            .then(response => {
                this.optionsEducation = response.data
            })
            .catch(error => {
                console.log('message : ' + error)
            })
        },
        getCompanyList(){
            api.get('/master/getcompany',{
                headers: {
                Authorization : localStorage.getItem("user-token")
                }
            })
            .then(response => {
                this.optionsCompany = response.data
            })
            .catch(error => {
                console.log('message : ' + error)
            })
        },
        confirmResign(){
            this.$q.loading.show()
            if (this.resigndate != '') {
                this.$q.loading.hide()
                this.icon = true
            } else {
                this.$q.loading.hide()
                this.$q.notify({
                    message: 'Please Complete the Form',
                    color: 'red',
                    icon: 'warning'
                })
            }
        },
        doResign(){
            this.$q.loading.show()
            var body = {
                'employeeid' : this.employeeid,
                'callsign' : this.callsign,
                'nikktp' : this.nik_ktp,
                'nik' : this.nik,
                'fullname' : this.fullname,
                'companyid' : this.company.companyid,
                'regionalid' : this.regional.regionalid,
                'mainjobid' : this.mainjob.mainjobid,
                'divisionid' : this.division.divisionid,
                'positionid' : this.position.positionid,
                'levelid' : this.level.levelid,
                'educationid' : this.education.eduid,
                'joindate' : this.joindate,
                'resigndate' : this.formatDate(this.resigndate),
                'joincallsign' : this.joincallsign,
                'releasecallsign' : this.releasecallsign,
                'address' : this.address,
                'email' : this.email,
                'phonenum' : this.phonenum,
                'remark' : this.remark,
                'remark_resign' : this.remark_resign,
                'remark_promote' : this.remark_promote,
                'remark_movement' : this.remark_movement,
                'remark_inactive' : null,
                'statusid' : 1,
                'createdby' : localStorage.getItem('user-username')
            }
            api.patch('/employee/updateemployeeresign', body, {
                headers : {
                    Authorization : localStorage.getItem('user-token')
                }
            })
            .then(response => {
                // console.log(response.data)
                var res = response.data  
                if (res.employeeid != null) {
                    this.$q.loading.hide()
                    this.$q.notify({
                        message: 'Employee Resign Success',
                        color: 'secondary',
                        icon: 'check_circle'
                    })
                    this.icon = false
                    this.$router.push('/employee')
                } else {
                    this.$q.loading.hide()
                    this.$q.notify({
                        message: 'Employee Resign Failed',
                        color: 'red',
                        icon: 'warning'
                    })  
                }
            })
            .catch(error => {
                this.$q.loading.hide()
                this.$q.notify({
                    message : error,
                    color: 'red',
                    icon: 'warning'
                })
                console.log('message : ' + error)
            })
        },
        formatDate (date) {
            var formatDate = date.replace(/\//g, '-')
            var datearray = formatDate.split('-')
            var newdate = datearray[2] + '-' + datearray[1] + '-' + datearray[0]
            return newdate
        }
    },
    beforeMount(){
        this.getEmployeeDetails()
        this.getLevelList()
        this.getEducationList()
        this.getRegionalList()
        this.getDivisionList()
        this.getMainJobList()
        this.getPositionList()
        this.getCompanyList()

        
    }
}