import { ref } from 'vue'
import {api} from 'boot/axios'
import { useQuasar } from 'quasar'

export default {
    setup (){
        const $q = useQuasar()
    },
    data () {
        return {
            tab: ref(this.$route.params.action),
            employeeid: ref(''),
            callsign: ref(''),
            nik: ref(''),
            nik_ktp: ref(''),
            fullname: ref(''),
            company: ref(''),
            regional: ref(''),
            division: ref(''),
            mainjob: ref(''),
            position: ref(''),
            joindate: ref(''),
            joincallsign: ref(''),
            releasecallsign: ref(''),
            address: ref(''),
            education: ref(''),
            level: ref(''),
            phonenum: ref(''),
            email: ref(''),
            remark: null,
            remark_resign: null,
            remark_promote: null,
            remark_movement: null,
            optionsCompany: [],
            optionsRegional: [],
            optionsDivision: [],
            optionsPosition: [],
            optionsMainjob: [],
            optionsEducation: [],
            optionsLevel: [],
            updateCallsign: {
                employeeid: ref(''),
                callsign: ref(''),
                fullname: ref(''),
                position: null,
                level: null,
                newcallsign: ref(''),
                newfullname: ref(''),
                newposition: null,
                newlevel: null
            },
            promote:{
                employeeid: ref(''),
                callsign: ref(''),
                fullname: ref(''),
                regional: null,
                division: null,
                mainjob: null,
                position: null,
                level: null,
                newcallsign: ref(''),
                newfullname: ref(''),
                newlevel: null,
                newregional: null,
                newdivision: null,
                newmainjob: null,
                newposition: null,
            },
            movement: {
                employeeid: ref(''),
                callsign: ref(''),
                fullname: ref(''),
                nik: ref(''),
                company: null,
                regional: null,
                division: null,
                mainjob: null,
                position: null,
                level: null,
                newcallsign: ref(''),
                newfullname: ref(''),
                newnik: ref(''),
                newlevel: null,
                newcompany: null,
                newregional: null,
                newdivision: null,
                newmainjob: null,
                newposition: null
            }
        }
    },
    methods:{
        getEmployeeDetails(){
            api.get('/employee/getemployeebyid', {
                headers: {
                    Authorization : localStorage.getItem('user-token'),
                    "Access-Control-Allow-Origin" : "http://localhost:8080"
                },
                params : {
                    'id' : this.$route.params.id
                }
            })
            .then(response => {
                if (response.data != '') {
                    this.detailsData(response.data)  
                    this.callsignData(response.data)
                    this.promoteData(response.data)
                    this.movementData(response.data)
                } else {
                    this.$q.notify({
                        message: 'Employee Data Not Found',
                        color: 'red',
                        icon: 'warning'
                    })
                    this.$router.push('/employee')
                }
            })
            .catch(error => {
                console.log('message : ' + error)
                this.$q.notify({
                    message: error,
                    color: 'red',
                    icon: 'warning'
                })
                this.$router.push('/employee')
            })
        },
        detailsData(response) {
            var data = response
            this.employeeid = data.employeeid
            this.callsign = data.callsign
            this.nik_ktp = data.nikktp
            this.nik = data.nik
            this.fullname = data.fullname
            this.address = data.address
            this.education = data.education
            this.phonenum = data.phonenum
            this.regional = data.regional
            this.division = data.division
            this.mainjob = data.mainJob
            this.position = data.position
            this.level = data.level
            this.joindate = data.joindate
            this.joincallsign = data.joincallsign
            this.company = data.company
            this.email = data.email
            this.remark = data.remark
            this.remark_resign = data.remark_resign
            this.remark_promote = data.remark_promote
            this.remark_movement = data.remark_movement
        },
        callsignData(response) {
            var data = response
            this.updateCallsign.employeeid = data.employeeid
            this.updateCallsign.callsign = data.callsign
            this.updateCallsign.fullname = data.fullname
            this.updateCallsign.position = data.position
            this.updateCallsign.level = data.level
        },
        promoteData(response) {
            var data = response
            this.promote.employeeid = data.employeeid
            this.promote.callsign = data.callsign
            this.promote.fullname = data.fullname
            this.promote.regional = data.regional
            this.promote.division = data.division
            this.promote.mainjob = data.mainJob
            this.promote.position = data.position
            this.promote.level = data.level
        },
        movementData(response) {
            var data = response
            this.movement.employeeid = data.employeeid
            this.movement.callsign = data.callsign
            this.movement.fullname = data.fullname
            this.movement.nik = data.nik
            this.movement.company = data.company
            this.movement.regional = data.regional
            this.movement.division = data.division
            this.movement.mainjob = data.mainJob
            this.movement.position = data.position
            this.movement.level = data.level
        },
        getPositionList(){
            api.get('/master/getallposition',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsPosition = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getMainJobList(){
            api.get('/master/getallmainjob',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsMainjob = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getDivisionList(){
            api.get('/master/getalldivision',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsDivision = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getRegionalList(){
            api.get('/master/getallregional',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsRegional = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getLevelList(){
            api.get('/master/getalllevel',{
              headers: {
                Authorization : localStorage.getItem("user-token")
              }
            })
            .then(response => {
              this.optionsLevel = response.data
            })
            .catch(error => {
              console.log('message : ' + error)
            })
        },
        getEducationList(){
            api.get('/master/getalledu',{
                headers: {
                Authorization : localStorage.getItem("user-token")
                }
            })
            .then(response => {
                this.optionsEducation = response.data
            })
            .catch(error => {
                console.log('message : ' + error)
            })
        },
        getCompanyList(){
            api.get('/master/getcompany',{
                headers: {
                Authorization : localStorage.getItem("user-token")
                }
            })
            .then(response => {
                this.optionsCompany = response.data
            })
            .catch(error => {
                console.log('message : ' + error)
            })
        },
        onUpdateDetails(){
            this.$q.loading.show()
            if (this.nik_ktp != '' && this.nik != '' && this.fullname != '' && this.address != '' && this.education != '' && this.phonenum != '' && this.email != '') {
                var body = {
                    'employeeid' : this.employeeid,
                    'callsign' : this.callsign,
                    'nikktp' : this.nik_ktp,
                    'nik' : this.nik,
                    'fullname' : this.fullname,
                    'companyid' : this.company.companyid,
                    'regionalid' : this.regional.regionalid,
                    'mainjobid' : this.mainjob.mainjobid,
                    'divisionid' : this.division.divisionid,
                    'positionid' : this.position.positionid,
                    'levelid' : this.level.levelid,
                    'educationid' : this.education.eduid,
                    'joindate' : this.joindate,
                    'resigndate' : this.resigndate,
                    'joincallsign' : this.joincallsign,
                    'releasecallsign' : this.releasecallsign,
                    'address' : this.address,
                    'email' : this.email,
                    'phonenum' : this.phonenum,
                    'remark' : this.remark,
                    'remark_resign' : this.remark_resign,
                    'remark_promote' : this.remark_promote,
                    'remark_movement' : this.remark_movement,
                    'remark_inactive' : null,
                    'statusid' : 2,
                    'createdby' : localStorage.getItem('user-username'),
                    'replacementemployeeid': null
                }
                api.patch('/employee/updateemployeedetails', body, {
                    headers : {
                        Authorization : localStorage.getItem('user-token')
                    }
                })
                .then(response => {
                    // console.log(response.data)
                    var res = response.data
                    if (res.fullname != null){
                        if(res.nikktp == null) {
                            this.$q.loading.hide()
                            this.$q.notify({
                                message : res.fullname,
                                color: 'red',
                                icon: 'warning'
                            })
                        } else {
                            this.$q.loading.hide()
                            this.$q.notify({
                                message : 'Employee Details Updated',
                                color: 'secondary',
                                icon: 'check_circle'
                            })
                            this.$router.push('/employee')
                        }
                    } else {
                        this.$q.loading.hide()
                        this.$q.notify({
                            message : 'Failed to Update Employee Details',
                            color: 'red',
                            icon: 'warning'
                        })
                    }
                    
                })
                .catch(error => {
                    this.$q.loading.hide()
                    this.$q.notify({
                        message : error,
                        color: 'red',
                        icon: 'warning'
                    })
                    console.log('message : ' + error)
                })
            } else {
                this.$q.notify({
                    message : 'Please Complete the Form',
                    color: 'red',
                    icon: 'warning'
                })
            }
        },
        onUpdateCallsign(){
            this.$q.loading.show()
            if(this.updateCallsign.newfullname != '' && this.updateCallsign.newcallsign != '' && this.updateCallsign.newposition != null && this.updateCallsign.newlevel != null){
                if (this.updateCallsign.newcallsign !=  this.updateCallsign.callsign) {
                    var body = {
                        'employeeid' : this.updateCallsign.employeeid,
                        'callsign' : this.updateCallsign.newcallsign,
                        'nikktp' : null,
                        'nik' : null,
                        'fullname' : this.updateCallsign.newfullname,
                        'companyid' : null,
                        'regionalid' : null,
                        'divisionid' : null,
                        'mainjobid' : null,
                        'positionid' : this.updateCallsign.newposition.positionid,
                        'levelid' : this.updateCallsign.newlevel.levelid,
                        'educationid' : null,
                        'joindate' : null,
                        'resigndate' : null,
                        'joincallsign' : null,
                        'releasecallsign' : null,
                        'address' : null,
                        'email' : null,
                        'phonenum' : null,
                        'remark' : null,
                        'remark_resign' : null,
                        'remark_promote' : null,
                        'remark_movement' : null,
                        'remark_inactive' : null,
                        'statusid' : null,
                        'createdby' : localStorage.getItem('user-username'),
                        'replacementemployeeid': null
                    }
                    api.patch('/employee/updateemployeecallsign', body, {
                        headers : {
                            Authorization : localStorage.getItem('user-token')
                        }
                    })
                    .then(response => {
                        var data = response.data
                        if(data.datecreated != null){
                            this.$q.loading.hide()
                            this.$q.notify({
                                message: 'Employee Callsign Updated',
                                color: 'secondary',
                                icon: 'check_circle'
                            })
                            this.$router.push('/employee')
                        } else {
                            this.$q.loading.hide()
                            this.$q.notify({
                                message: data.fullname,
                                color: 'red',
                                icon: 'warning'
                            })
                        }
                    })
                    .catch(error => {
                        this.$q.loading.hide()
                        console.log('message : ' + error)
                    })
                } else {
                    this.$q.loading.hide()
                    this.$q.notify({
                        message: 'New Callsign Needed',
                        color: 'red',
                        icon: 'warning'
                    })
                }
            } else {
                this.$q.loading.hide()
                this.$q.notify({
                    message: 'Please Complete the Form',
                    color: 'red',
                    icon: 'warning'
                })
            }
        },
        onPromote(){
            this.$q.loading.show()
            if(this.promote.newcallsign != '' && this.promote.newfullname != '' && this.promote.newregional != null && this.promote.newdivision != null && this.promote.newmainjob != null && this.promote.newposition != null && this.promote.newlevel != null){
                if (this.promote.position.positionname == this.promote.newposition.positionname && this.promote.level.levelname == this.promote.newlevel.levelname ){
                    this.$q.loading.hide()
                    this.$q.notify({
                        message: 'New Level Required',
                        color: 'red',
                        icon: 'warning'
                    })
                } else {
                    var body = {
                        'employeeid' : this.promote.employeeid,
                        'callsign' : this.promote.newcallsign,
                        'nikktp' : null,
                        'nik' : null,
                        'fullname' : this.promote.newfullname,
                        'companyid' : null,
                        'regionalid' : this.promote.newregional.regionalid,
                        'divisionid' : this.promote.newdivision.divisionid,
                        'mainjobid' : this.promote.newmainjob.mainjobid,
                        'positionid' : this.promote.newposition.positionid,
                        'levelid' : this.promote.newlevel.levelid,
                        'educationid' : null,
                        'joindate' : null,
                        'resigndate' : null,
                        'joincallsign' : null,
                        'releasecallsign' : null,
                        'address' : null,
                        'email' : null,
                        'phonenum' : null,
                        'remark' : null,
                        'remark_resign' : null,
                        'remark_promote' : this.remark_promote,
                        'remark_movement' : null,
                        'remark_inactive' : null,
                        'remark_blacklist' : null,
                        'statusid' : null,
                        'createdby' : localStorage.getItem('user-username'),
                        'replacementemployeeid': null
                    }
                    api.patch('/employee/updateemployeepromote', body, {
                        headers: {
                            Authorization: localStorage.getItem('user-token')
                        }
                    })
                    .then(response => {
                        var data = response.data
                        if (data.datecreated != null) {
                            this.$q.loading.hide()
                            this.$q.notify({
                                message: 'Employee Promoted',
                                color: 'secondary',
                                icon: 'check_circle'
                            })
                            this.$router.push('/employee')
                        } else {
                            this.$q.loading.hide()
                            this.$q.notify({
                                message: data.fullname,
                                color: 'red',
                                icon: 'warning'
                            })
                        }
                    })
                    .catch(error => {
                        this.$q.loading.hide()
                        this.$q.notify({
                            message: error,
                            color: 'red',
                            icon: 'warning'
                        })
                    })
                }
            } else {
                this.$q.loading.hide()
                this.$q.notify({
                    message: 'Please Complete the Form',
                    color: 'red',
                    icon: 'warning'
                })
            }
        },
        onMovement(){
            this.$q.loading.show()
            if (this.movement.newcallsign != '' && this.movement.newfullname != '' && this.movement.newnik != '' && this.movement.newcompany != null && this.movement.newregional != null && this.movement.newdivision != null && this.movement.newmainjob != null && this.movement.newposition != null && this.movement.newlevel != null ) {
                if (this.movement.newcompany.companyname != this.movement.company.companyname) {
                    var body = {
                        'employeeid' : this.movement.employeeid,
                        'callsign' : this.movement.newcallsign,
                        'nikktp' : null,
                        'nik' : this.movement.newnik,
                        'fullname' : this.movement.newfullname,
                        'companyid' : this.movement.newcompany.companyid,
                        'regionalid' : this.movement.newregional.regionalid,
                        'divisionid' : this.movement.newdivision.divisionid,
                        'mainjobid' : this.movement.newmainjob.mainjobid,
                        'positionid' : this.movement.newposition.positionid,
                        'levelid' : this.movement.newlevel.levelid,
                        'educationid' : null,
                        'joindate' : null,
                        'resigndate' : null,
                        'joincallsign' : null,
                        'releasecallsign' : null,
                        'address' : null,
                        'email' : null,
                        'phonenum' : null,
                        'remark' : null,
                        'remark_resign' : null,
                        'remark_promote' : null,
                        'remark_movement' : this.remark_movement,
                        'remark_inactive' : null,
                        'statusid' : null,
                        'createdby' : localStorage.getItem('user-username'),
                        'replacementemployeeid': null
                    }
                    api.patch('/employee/updateemployeemovement', body, {
                        headers: {
                            Authorization: localStorage.getItem('user-token')
                        }
                    })
                    .then(response => {
                        var data = response.data
                        if (data.datecreated != null) {
                            this.$q.loading.hide()
                            this.$q.notify({
                                message: 'Employee Moved',
                                color: 'secondary',
                                icon: 'check_circle'
                            })
                            this.$router.push('/employee')
                        } else {
                            this.$q.loading.hide()
                            this.$q.notify({
                                message: data.fullname,
                                color: 'red',
                                icon: 'warning'
                            })
                        }
                    })
                    .catch(error => {
                        this.$q.loading.hide()
                        this.$q.notify({
                            message: error,
                            color: 'red',
                            icon: 'warning'
                        })
                    })
                } else {
                    this.$q.loading.hide()
                    this.$q.notify({
                        message: 'New Company Required',
                        color: 'red',
                        icon: 'warning'
                    })
                }
            } else {
                this.$q.loading.hide()
                this.$q.notify({
                    message: 'Please Complete the Form',
                    color: 'red',
                    icon: 'warning'
                })
            }
        }
    },
    beforeMount(){
        this.getEmployeeDetails()
        this.getLevelList()
        this.getEducationList()
        this.getRegionalList()
        this.getDivisionList()
        this.getMainJobList()
        this.getPositionList()
        this.getCompanyList()

        
    }
}