const columns = [
  {
    name: 'no',
    required: true,
    label: 'No',
    align: 'center',
    field: row => row.name,
    format: val => `${val}`,
    sortable: true
  },
  { name: 'username', label: 'Username', field: 'username', sortable: true, align: 'center'},
  { name: 'fullname', label: 'Full Name', field: 'fullname', sortable: true, align: 'center' },
  { name: 'company', label: 'Company', field: 'company', align: 'center' },
  { name: 'regional', label: 'Regional', field: 'regional', align: 'center' },
  { name: 'role', label: 'Role', field: 'role', sortable: true, align: 'center' },
  { name: 'action', label: 'Action', field: 'action', sortable: true, align: 'center' }
]

import { ref } from 'vue'
import {api} from 'boot/axios'
import { useQuasar } from "quasar";

export default {
  setup(){
    const $q = useQuasar()
  },
  data () {
    return {
      filter: ref(''),
      icon: ref(false),
      icondelete: ref(false),
      iconview: ref(false),
      useriddelete: ref(''),
      fullnamedelete: ref(''),
      user : {
        company: ref(''),
        regional: ref(''),
        role: ref(''),
        username: ref(''),
        fullName: ref(''),
        password: ref(''),
        passwordconf: ref(''),
      },
      userdetails : {
        userid: ref(''),
        company: null,
        regional: null,
        role: null,
        username: ref(''),
        fullname: ref('')
      },
      columns,
      rows: [],
      companyOptions: [],
      regionalOptions: [],
      roleOptions: [],

    }
  },
  methods: {
    getUserList(){
      api.get('/master/getusers', {
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        var datas = response.data
        var i = 0;
        for (var key in datas) {
          datas[i]['no'] = i + 1
          i++

        }
        this.rows = datas
      })
      .catch(error => {
        console.log('message : ' +error)
      })
    },
    getCompanyList(){
      api.get('/master/getcompany',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.companyOptions = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getRegionalList(){
      api.get('/master/getallregional',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.regionalOptions = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getRoleList(){
      api.get('/master/getroles',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.roleOptions = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    checkUsername(username) {
      api.get('/master/getuserbyusername', {
        headers: {
          Authorization : localStorage.getItem('user-token')
        },
        params: {
          username : username
        }
      })
      .then(response => {
        if (response != null) {
          localStorage.setItem('checkusername', 'available')
        } else {
          localStorage.setItem('checkusername', 'used')
        }
      })
      .catch(error => {
        console.log('message : ' + error)
        localStorage.setItem('checkusername', 'error')
      })
    },
    onSubmit () {
      this.$q.loading.show()
      if (this.user.username != '' && this.user.fullName != '' && this.user.company != '' && this.user.regional != '' && this.user.role != '' && this.user.password != '' && this.user.passwordconf != '') {
        if (this.user.password == this.user.passwordconf) {
          var body = {
            'userid' : null,
            'roleid' : this.user.role.roleid,
            'companyid' : this.user.company.companyid,
            'regionalid' : this.user.regional.regionalid,
            'username' : this.user.username,
            'password' : this.user.password,
            'fullname' : this.user.fullName
          }
          api.post('/master/createuser', body, {
            headers: {
              Authorization : localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var userCreated = response.data 
            if (userCreated.fullname != null) {
              if (userCreated.accountcreated == null) {
                this.$q.notify({
                  message: userCreated.fullname,
                  color: 'red',
                  icon: 'warning'
                })
                localStorage.removeItem('checkusername')
                this.getUserList()
                this.$q.loading.hide()
              } else {
                this.$q.notify({
                  message: 'New Account Created',
                  color: 'secondary',
                  icon: 'check_circle'
                })
                localStorage.removeItem('checkusername')
                this.onReset()
                this.icon = false
                this.getUserList()
                this.$q.loading.hide()
              }
            } else {
              this.$q.notify({
                message: 'Failed to Create New Account',
                color: 'red',
                icon: 'warning'
              })
              localStorage.removeItem('checkusername')
              this.getUserList()
              this.$q.loading.hide()
            }
          })
          .catch(error => {
            console.log('message : ' + error)
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Password not Match',
            color: 'red',
            icon: 'warning'
          })
        }
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red ',
          icon: 'warning'
        })
      }

    },
    onReset () {
      this.username = null
      this.fullName = null
      this.password = null
      this.passwordconf = null
      this.company = null
      this.role = null
      this.regional = null
    },
    doUpdateUser (id) {
      api.get('/master/useruserbyid',{
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        this.iconview = true
        var data = response.data
        this.userdetails.userid = data.userid
        this.userdetails.username = data.username
        this.userdetails.fullname = data.fullname
        this.userdetails.company = data.company
        this.userdetails.regional = data.regional
        this.userdetails.role = data.role
      })
      .catch(error => {
        console.log('message : ' + error)
        this.iconview = false
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    updateAccount() {
      this.$q.loading.show()
      if (this.userdetails.username != '' && this.userdetails.fullname != '' && this.userdetails.company != '' && this.userdetails.regional != '' && this.userdetails.role != ''){
        var body = {
          'userid' : this.userdetails.userid,
          'username' : this.userdetails.username,
          'fullname' : this.userdetails.fullname,
          'password' : this.userdetails.password,
          'companyid' : this.userdetails.company.companyid,
          'regionalid' : this.userdetails.regional.regionalid,
          'roleid' : this.userdetails.role.roleid
        }
        api.patch('/master/updateuser', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if(data.fullname != null){
            if(data.accountcreated != null){
              this.$q.loading.hide()
              this.iconview = false
              this.getUserList()
              this.$q.notify({
                message: 'Account Updated',
                color: 'secondary',
                icon: 'check_circle'
              })
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.fullname,
                color: 'red',
                icon: 'warning'
              })
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Update Account Details',
              color: 'red',
              icon: 'warning'
            })
          }          
        })
        .catch(error => {
          this.$q.loading.hide()
          console.log('message : '+error)
          this.iconview = false
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    doDeleteUser (id) {
      api.get('/master/useruserbyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params:{
          'id' : id
        }
      })
      .then(response => {
        var data = response.data
        this.useriddelete = data.userid
        this.fullnamedelete = data.fullname
        this.icondelete = true
      })
      .catch(error => {
        console.log('message : ' + error)
        this.icondelete = false
      })
    },
    deleteAccount(id) {
      this.$q.loading.show()
      api.delete('/master/deleteuser',{
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        if(response.data == 1) {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Account Deleted',
            color: 'secondary',
            icon: 'check_circle'
          })
          this.icondelete = false
          this.getUserList()
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Failed to Delete Account',
            color: 'red',
            icon: 'warning'
          })
          this.icondelete = false
          this.getUserList()
        }
      })
      .catch(error => {
        console.log('message : ' +error)
        this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.icondelete = false
          this.getUserList()
      })
    }
  },
  beforeMount(){
    this.getUserList()
    this.getCompanyList()
    this.getRegionalList()
    this.getRoleList()
  }
}