const columnsposition = [
    {
      name: 'no',
      required: true,
      label: 'No',
      align: 'center',
      sortable: true
    },
    { name: 'mainJob', align: 'center', label: 'Main Job', field: 'mainJob', sortable: true },
    { name: 'positionname', align: 'center', label: 'Position', field: 'positionname', sortable: true },
    { name: 'positionid', label: 'Action', field: 'positionid', align: 'center' }
  ]

import {ref} from 'vue'
import {api} from 'boot/axios'
import { useQuasar } from 'quasar'

export default {
  setup () {
    const $q = useQuasar()
  },
  data () {
    return {
      filterposition: ref(''),
      iconposition: ref(false),
      icondelete: ref(false),
      iconview: ref(false),
      position: ref(''),
      columnsposition,
      rowsposition : [],
      mainjob: null,
      mainjobOptions: [],
      del: {
        positionid: ref (''),
        recordposition: ref('')
      },
      upd: {
        positionid : ref(''),
        positionname: ref(''),
        mainjob : null
      }
    }
  },
  methods: {
    getPositionList() {
      api.get('/master/getallposition', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        var data = response.data
        var i = 0
        for (var key in data){
          data[i]['no'] = i + 1
          i++
        }
        this.rowsposition = data
      })
      .catch(error => {
        console.log('message : '+error)
      })
    },
    getMainJobOptions() {
      api.get('/master/getallmainjob', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        this.mainjobOptions = response.data
      })
      .catch(error => {
        console.log('message : '+ error )
      })
    },
    onSubmitPosition () {
      this.$q.loading.show()
      if (this.mainjob != null && this.position != '') {
        var body = {
          'positionid' : null,
          'mainjobid' : this.mainjob.mainjobid,
          'positionname' : this.position
        }
        api.post('/master/createposition', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if (data.positionname != null) {
            if (data.datecreated != null) {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Position Created',
                color: 'secondary',
                icon: 'check_circle'
              })  
              this.iconposition = false
              this.getPositionList()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.positionname,
                color: 'red',
                icon: 'warning'
              })  
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Create Position',
              color: 'red',
              icon: 'warning'
            })  
            this.iconposition = false
            this.getPositionList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onResetPosition () {
        this.mainjob = null
        this.position = null
    },
    doUpdatePosition (id) {
      this.$q.loading.show()
      api.get('/master/getpositionbyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id':id
        }
      })
      .then(response => {
        var data = response.data
        this.upd.positionid = data.positionid
        this.upd.positionname = data.positionname
        this.upd.mainjob = data.mainJob
        this.iconview = true
        this.$q.loading.hide()
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    doDeletePosition (id) {
      this.$q.loading.show()
      api.get('/master/getpositionbyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id':id
        }
      })
      .then(response => {
        var data = response.data
        this.del.positionid = data.positionid
        this.del.recordposition = data.mainJob.mainjobname + ' - ' + data.positionname
        this.icondelete = true
        this.$q.loading.hide()
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    deletePosition(id) {
      this.$q.loading.show()
      api.delete('/master/deleteposition', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id':id
        }
      })
      .then(response => {
        if (response.data == 1) {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Record Deleted',
            color: 'secondary',
            icon: 'check_circle'
          })
          this.icondelete = false
          this.getPositionList()
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Failed to Delete Record',
            color: 'red',
            icon: 'warning'
          })
          this.icondelete = false
          this.getPositionList()
        }
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
        this.icondelete = false
      })
    },
    updatePosition(){
      this.$q.loading.show()
      if (this.upd.positionname != '' && this.upd.mainjob != null){
        var body = {
          'positionid' : this.upd.positionid,
          'positionname' : this.upd.positionname,
          'mainjobid' : this.upd.mainjob.mainjobid
        }
        api.patch('/master/updateposition', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if (data.positionname != null) {
            if (data.datecreated != null) {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Record Updated',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconview = false
              this.getPositionList()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.positionname,
                color: 'red',
                icon: 'warning'
              })
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Update Record',
              color: 'red',
              icon: 'warning'
            })
            this.iconview = false
            this.getPositionList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.iconview = false
          this.getPositionList()
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    }
  },
  beforeMount () {
    this.getPositionList()
    this.getMainJobOptions()
  }
}