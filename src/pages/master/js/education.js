const columnseducation = [
  {
    name: 'no',
    required: true,
    label: 'No',
    align: 'center',
    sortable: true
  },
  { name: 'educationname', align: 'center', label: 'Education', field: 'educationname', sortable: true },
  { name: 'eduid', label: 'Action', field: 'eduid', align: 'center' }
]

import { api } from 'src/boot/axios'
import { ref } from 'vue'
import { useQuasar } from 'quasar'

export default {
    setup(){
      const $q = useQuasar()
    },
    data () {
      return {
        filtereducation: ref(''),
        iconeducation: ref(false),
        icondelete: ref(false),
        iconview: ref(false),
        columnseducation,
        rowseducation: [],
        education: ref(''),
        edu: {
          eduid: ref(''),
          educationname: ref('')
        }
      }
    },
    methods: {
      getEducationList(){
        api.get('/master/getalledu',{
          headers:{
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response =>{
          var data = response.data
          var i = 0
          for (var key in data) {
            data[i]['no'] = i + 1
            i++
          }
          this.rowseducation = data
        })
        .catch(error => {
          console.log('message : '+error)
        })
      },
      onSubmitEducation () {
        this.$q.loading.show()  
        if (this.education != ''){
          var body = {
            'eduid' : null,
            'educationname' : this.education
          }
          api.post('/master/createedu', body, {
            headers: {
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var data = response.data
            if(data.educationname != null){
              if(data.datecreated != null){
                this.$q.loading.hide()  
                this.$q.notify({
                  message: 'New Education Created',
                  color: 'secondary',
                  icon: 'check_circle'
                })
                this.iconeducation = false
                this.getEducationList()
                this.onResetEducation()
              } else {
                this.$q.loading.hide()  
                this.$q.notify({
                  message: data.educationname,
                  color: 'red',
                  icon: 'warning'
                })
              }
            } else {
              this.$q.loading.hide()  
              this.iconeducation = false
              this.$q.notify({
                message: 'Failed to Create New Education',
                color: 'red',
                icon: 'warning'
              })
            }
          })
          .catch(error => {
            this.$q.loading.hide()  
            this.iconeducation = false
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            })
          })
        }else{
          this.$q.loading.hide()  
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      },
      onResetEducation () {
        this.education = null
      },
      doUpdateEducation (id) {
        api.get('/master/getedubyid', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          },
          params:{
            'id':id
          }
        })
        .then(response => {
          var data = response.data
          this.edu.eduid = data.eduid
          this.edu.educationname = data.educationname
          this.iconview = true
        })
        .catch(error => {
          this.iconview = false
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      doDeleteEducation (id) {
        api.get('/master/getedubyid', {
          headers:{
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id':id
          }
        })
        .then(response => {
          var data = response.data
          this.edu.eduid = data.eduid
          this.edu.educationname = data.educationname
          this.icondelete = true
        })
        .catch(error => {
          this.$q.notify({
            message: error,
            color: 'error',
            icon: 'warning'
          })
          this.icondelete = false
        })
      },
      deleteEducation(id){
        this.$q.loading.show()
        api.delete('/master/deleteedu',{
          headers:{
            Authorization: localStorage.getItem('user-token')
          },
          params:{
            'id':id
          }
        })
        .then(response => {
          if (response.data == 1){
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Education Deleted',
              color: 'secondary',
              icon: 'check_circle'
            })
            this.icondelete = false
            this.getEducationList()
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Delete Data',
              color: 'red',
              icon: 'warning'
            })
            this.icondelete = false
            this.getEducationList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      updateEducation(){
        this.$q.loading.show()
        if(this.edu.educationname != ''){
          var body = {
            'eduid' : this.edu.eduid,
            'educationname' : this.edu.educationname
          }
          api.patch('/master/updateedu', body, {
            headers: {
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var data = response.data
            if (data.educationname != null){
              if (data.datecreated != null){
                this.$q.loading.hide()
                this.$q.notify({
                  message: 'Education Updated',
                  color: 'secondary',
                  icon: 'check_circle'
                })
                this.iconview = false
                this.getEducationList()
              } else {
                this.$q.loading.hide()
                this.$q.notify({
                  message: data.educationname,
                  color: 'red',
                  icon: 'warning'
                })  
              }
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Failed to Update Education',
                color: 'red',
                icon: 'warning'
              })
            }
          })
          .catch(error =>{
            this.$q.loading.hide()
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            })
            this.iconview = false
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Fill the Form',
            color: 'red',
            icon: 'warning'
          })          
        }
      }
    },
    beforeMount () {
      this.getEducationList()
    }
  }