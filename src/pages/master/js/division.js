const columns = [
    {
      name: 'no',
      required: true,
      label: 'No',
      align: 'center',
      sortable: true
    },
    { name: 'divisioname', align: 'center', label: 'Division', field: 'divisioname', sortable: true },
    { name: 'divisionid', label: 'Action', field: 'divisionid', align: 'center' }
  ]
import { api } from 'src/boot/axios'
import {ref} from 'vue'

export default {
  
    data () {
      return {
        filter: ref(''),
        icon: ref(false),
        icondelete: ref(false),
        iconview: ref(false),
        division: ref(''),
        div: {
          divisionid: ref(''),
          divisioname: ref('')
        },
        columns,
        rows: []
      }
    },
    methods: {
      getDivisionList(){
        api.get('/master/getalldivision', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          var i = 0;
          for (var key in data) {
            data[i]['no'] = i + 1
            i++
          }
          this.rows = data
        })
        .catch(error => {
          console.log('message : '+error)
        })
      },
      onSubmit () {
        this.$q.loading.show()
        if (this.division != ''){
          var body = {
            'divisionid' : null,
            'divisionname' : this.division
          }
          api.post('/master/createdivision', body, {
            headers:{
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var data = response.data
            if(data.datecreated != null){
              this.icon = false
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Division Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.getDivisionList()
              this.onReset()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.divisioname,
                color: 'red',
                icon: 'warning'
              })
            }
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      },
      onReset () {
        this.division = null
      },
      doUpdate (id) {
        api.get('/master/getdivisionbyid', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id': id
          }
        })
        .then(response => {
          var data = response.data
          this.div.divisionid = data.divisionid
          this.div.divisioname = data.divisioname
          this.iconview = true
        })
        .catch(error => {
          this.iconview = false
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      doDelete (id) {
        api.get('/master/getdivisionbyid', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id': id
          }
        })
        .then(response => {
          var data = response.data
          this.div.divisionid = data.divisionid
          this.div.divisioname = data.divisioname
          this.icondelete = true
        })
        .catch(error => {
          this.icondelete = false
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      deleteDivision(id){
        api.delete('/master/deletedivision',{
          headers:{
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id':id
          }
        })
        .then(response => {
          if(response.data == 1){
            this.icondelete = false
            this.$q.notify({
              message: 'Division Deleted',
              color: 'secondary',
              icon: 'check_circle'
            })
            this.getDivisionList()
          } else {
            this.icondelete = false
            this.$q.notify({
              message: 'Failed to Delete',
              color: 'red',
              icon: 'warning'
            })
            this.getDivisionList()
          }
        })
        .catch(error => {
          this.icondelete = false
          this.getDivisionList()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      updateDivision(){
        this.$q.loading.show()
        if(this.div.divisioname != ''){
          var body = {
            'divisionid':this.div.divisionid,
            'divisionname':this.div.divisioname
          }
          api.patch('/master/updatedivision', body,{
            headers: {
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var data = response.data
            if(data.datecreated != null){
              this.$q.loading.hide()
              this.iconview = false
              this.$q.notify({
                message: 'Division Updated',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.getDivisionList()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.divisioname,
                color: 'red',
                icon: 'warning'
              })
            }
          })
          .catch(error =>{
            this.$q.loading.hide()
            this.iconview = false
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            })
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      }
    },
    beforeMount () {
      this.getDivisionList()
    }
  }