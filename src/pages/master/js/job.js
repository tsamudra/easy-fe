const columnsjob = [
  {
    name: 'no',
    required: true,
    label: 'No',
    align: 'center',
    sortable: true
  },
  { name: 'mainjobname', align: 'center', label: 'Main Job', field: 'mainjobname', sortable: true },
  { name: 'mainjobid', label: 'Action', field: 'mainjobid', align: 'center' }
]


import {ref} from 'vue'
import {api} from 'boot/axios'
import { useQuasar } from 'quasar'
export default {
  setup(){
    const $q = useQuasar()
  },
  data () {
    return {
      filterjob: ref(''),
      iconjob: ref(false),
      icondelete: ref(false),
      iconview: ref(false),
      mainjob: ref(''),
      columnsjob,
      rowsjob: [],
      mj: {
        mainjobid: ref(''),
        mainjobname: ref('')
      }
    }
  },
  methods: {
    getMainJobList(){
      api.get('/master/getallmainjob', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        var data = response.data
        var i = 0
        for (var key in data) {
          data[i]['no'] = i + 1
          i++
        }
        this.rowsjob = data
      })
      .catch(error => {
        console.log('message : '+error)
      })
    },
    onSubmitJob () {
      this.$q.loading.show()
      if ( this.mainjob != ''){
        var body = {
          'mainjobid': null,
          'mainjobname': this.mainjob
        }
        api.post('/master/createnewmainjob', body, {
          headers:{
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if (data.mainjobname != null){
            if(data.datecreated != null){
              this.$q.loading.hide()
              this.$q.notify({
                message : 'New Main Job Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconjob = false
              this.getMainJobList()
              this.onResetJob()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message : data.mainjobname,
                color: 'red',
                icon: 'warning'
              })
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message : 'Failed to Create Main Job',
              color: 'red',
              icon: 'warning'
            })
            this.iconjob = false
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message : error,
            color: 'red',
            icon: 'warning'
          })
          this.iconjob = false
        })
      }else{
        this.$q.loading.hide()
        this.$q.notify({
          message : 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onResetJob () {
      this.mainjob = null
    },
    doUpdateJob (id) {
      api.get('/master/getmainjobbyid',{
        headers:{
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id':id
        }
      })
      .then(response => {
        var data = response.data
        this.mj.mainjobid = data.mainjobid
        this.mj.mainjobname = data.mainjobname
        this.iconview = true
      })
      .catch(error => {
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    doDeleteJob (id) {
      api.get('/master/getmainjobbyid',{
        headers:{
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id':id
        }
      })
      .then(response => {
        var data = response.data
        this.mj.mainjobid = data.mainjobid
        this.mj.mainjobname = data.mainjobname
        this.icondelete = true
      })
      .catch(error => {
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    deleteMainJob(id){
      this.$q.loading.show()
      api.delete('/master/deletemainjob',{
        headers:{
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        if (response.data == 1){
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Main Job Deleted',
            color: 'secondary',
            icon: 'check_circle'
          })
          this.icondelete = false
          this.getMainJobList()
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Failed to Delete Main Job',
            color: 'red',
            icon: 'warning'
          })
          this.icondelete = false
          this.getMainJobList()
        }
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    updateMainJob(){
      this.$q.loading.show()
      if (this.mj.mainjobname != ''){
        var body = {
          'mainjobid' : this.mj.mainjobid,
          'mainjobname' : this.mj.mainjobname
        }
        api.patch('/master/updatemainjob', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if(data.mainjobname != null){
            if (data.datecreated != null){
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Main Job Updated',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconview = false
              this.getMainJobList()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.mainjobname,
                color: 'red',
                icon: 'warning'
              })
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed Update Main Job',
              color: 'red',
              icon: 'warning'
            })
            this.iconview = false
            this.getMainJobList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    }
  },
  beforeMount () {
    this.getMainJobList()
  }
}