const columnslevel = [
    {
      name: 'no',
      required: true,
      label: 'No',
      align: 'center',
      sortable: true
    },
    { name: 'position', align: 'center', label: 'Position', field: 'position', sortable: true },
    { name: 'level', label: 'Level', field: 'level', sortable: true, align: 'center' },
    { name: 'levellistid', label: 'Action', field: 'levellistid', align: 'center' }
  ]
  
  
import { api } from 'src/boot/axios'
import { ref } from 'vue'
import { useQuasar } from 'quasar'

export default {
  setup() {
    const $q = useQuasar()
  },
  data () {
    return {
      filterlevel: ref(''),
      iconlevel: ref(false),
      iconlevelnew: ref(false),
      iconview: ref(false),
      icondelete: ref(false),
      position: null,
      level: null,
      levelnew: ref(''),
      columnslevel,
      rowslevel: [],
      positionOptions: [],
      levelOptions: [],
      del: {
        levellistid: ref(''),
        recordlabel: ref('')
      },
      upd : {
        levellistid: ref(''),
        position: null,
        level: null
      }
    }
  },
  methods: {
    getLevelList(){
      api.get('/master/getalllevellist', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        var data = response.data
        var i = 0
        for(var key in data){
          data[i]['no'] = i + 1
          i++
        }
        this.rowslevel = data
      })
      .catch(error => {
        console.log('message : '+error)
      })
    },
    getLevelOptions(){
      api.get('/master/getalllevel', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        var data = response.data
        this.levelOptions = data
      })
      .catch(error => {
        console.log('message : '+error)
      })
    },
    getPositionOptions(){
      api.get('/master/getallposition', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        var data = response.data
        this.positionOptions = data
      })
      .catch(error => {
        console.log('message : '+error)
      })
    },
    onSubmit () {
      this.$q.loading.show()
      if (this.position != null && this.level != null){
        var body = {
          'levellistid' : null,
          'positionid' : this.position.positionid,
          'levelid': this.level.levelid
        }
        api.post('/master/createlevellist', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if (data.levellistid != null) {
            if (data.levellistid == 0) {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Record Registered. Cannot Save Duplicate Data',
                color: 'red',
                icon: 'warning'
              })
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Record Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconlevel = false
              this.getLevelList()
              this.onReset()
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Create New Record',
              color: 'red',
              icon: 'warning'
            })
            this.iconlevel = false
            this.getLevelList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.iconlevel = false
          this.getLevelList()
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onReset () {
      this.position = null
      this.level = null
    },
    onSubmitLevel () {
      this.$q.loading.show()
      if (this.levelnew != ''){
        var body = {
          'levelid' : null,
          'levelname' : this.levelnew
        }
        api.post('/master/createlevel', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          console.log(data)
          if (data.levelname != null){
            if (data.datecreated != null){
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Level Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconlevelnew = false
              this.getLevelOptions()
              this.onResetLevel()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.levelname,
                color: 'red',
                icon: 'warning'
              })
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Create New Level',
              color: 'red',
              icon: 'warning'
            })
            this.iconlevelnew = false
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.iconlevelnew = false
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onResetLevel () {
      this.levelnew = null
    },
    doUpdate (id) {
      this.$q.loading.show()
      api.get('/master/getlevellistbyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        var data = response.data
        this.upd.levellistid = data.levellistid
        this.upd.position = data.position
        this.upd.level = data.level
        this.$q.loading.hide()
        this.iconview = true
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    doDelete (id) {
      this.$q.loading.show()
      api.get('/master/getlevellistbyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        var data = response.data
        this.del.levellistid = data.levellistid
        this.del.recordlabel = data.position.positionname  + ' - ' + data.level.levelname
        this.$q.loading.hide()
        this.icondelete = true
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    deletePosition(id) {
      this.$q.loading.show()
      api.delete('/master/deletelevellist', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        if (response.data == 1) {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Record Deleted',
            color: 'secondary',
            icon: 'check_circle'
          })
          this.icondelete = false
          this.getLevelList()
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Failed to Delete Record',
            color: 'red',
            icon: 'warning'
          })
          this.icondelete = false
          this.getLevelList()
        }
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
        this.icondelete = false
          this.getLevelList()
      })
    },
    updatePosition(){
      this.$q.loading.show()
      if (this.upd.position != null && this.upd.level != null){
        var body = {
          'levellistid' : this.upd.levellistid,
          'positionid' : this.upd.position.positionid,
          'levelid' : this.upd.level.levelid
        }
        api.patch('/master/updatelevellist', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if ( data.levellistid != null){
            if (data.levellistid == 0) {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Record Registered. Cannot Save Duplicate Data',
                color: 'red',
                icon: 'warning'
              })
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Record Updated',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconview = false
              this.getLevelList()
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Update Record',
              color: 'red',
              icon: 'warning'
            })
            this.iconview = false
            this.getLevelList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.iconview = false
          this.getLevelList()
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    }
  },
  beforeMount () {
    this.getLevelList()
    this.getLevelOptions()
    this.getPositionOptions()
  }
}