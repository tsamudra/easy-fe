const columnsregional = [
  {
    name: 'no',
    required: true,
    label: 'No',
    align: 'center',
    sortable: true
  },
  { name: 'regional', align: 'center', label: 'Regional Dept', field: 'regional', sortable: true },
  { name: 'division', label: 'Division', field: 'division', sortable: true, align: 'center' },
  { name: 'regionallistid', label: 'Action', field: 'regionallistid', align: 'center' }
]


import { api } from 'src/boot/axios'
import { ref } from 'vue'

export default {
    data () {
      return {
        filterregional: ref(''),
        iconregional: ref(false),
        iconregionaldept: ref(false),
        icondelete: ref(false),
        iconview: ref(false),
        regional: ref(''),
        regionaldept: ref(''),
        reg: null,
        division: null,
        columnsregional,
        rowsregional: [],
        regionalOptions: [],
        divisionOptions: [],
        upd:{
          regionallistid: ref(''),
          regional: null,
          division: null
        },
        del: {
          delid: ref(''),
          delname: ref('')
        }
      }
    },
    methods: {
      getRegionalList(){
        api.get('/master/getallregionallist', {
          headers:{
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          var i = 0
          for (var key in data) {
            data[i]['no'] = i + 1
            i++
          }
          this.rowsregional = data
        })
        .catch(error => {
          console.log('message : '+ error)
        })
      },
      getRegionalOptions(){
        api.get('/master/getallregional',{
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          this.regionalOptions = response.data
        })
        .catch(error => {
          console.log('message : ' + error)
        })
      },
      getDivisionOptions(){
        api.get('/master/getalldivision',{
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          this.divisionOptions = response.data
        })
        .catch(error => {
          console.log('message : ' + error)
        })
      },
      onSubmitRegional () {
        this.$q.loading.show()
        if (this.reg != null && this.division != null){
          var body = {
            'regionallistid' : null,
            'regionalid' : this.reg.regionalid,
            'divisionid' : this.division.divisionid
          }
          api.post('/master/createregionallist', body, {
            headers : {
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var data = response.data
            if (data.regionallistid != 1){
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Record Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconregional = false
              this.getRegionalList()
              this.onResetRegional()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Record Registered. Cannot Save Duplicate Data',
                color: 'red',
                icon: 'warning'
              })
            }
          })
          .catch(error => {
            this.$q.loading.hide()
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            })
            this.iconregional = false
            this.onResetRegional()
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      },
      onResetRegional () {
        this.reg = null
        this.division = null
      },
      onSubmitRegionalDept () {
        this.$q.loading.show()
        if (this.regionaldept != '') {
          var body = {
            'regionaid' : null,
            'regionaldept' : this.regionaldept
          }
          api.post('/master/createregional', body, {
            headers: {
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var data = response.data
            if (data.regionaldept != null){
              if (data.datecreated != null){
                this.$q.loading.hide()
                this.$q.notify({
                  message: 'New Regional Dept Created',
                  color: 'secondary',
                  icon: 'check_circle'
                })
                this.getRegionalOptions()
                this.onResetRegionalDept()
                this.iconregionaldept = false
              } else {
                this.$q.loading.hide()
                this.$q.notify({
                  message: data.regionaldept,
                  color: 'red',
                  icon: 'warning'
                })
              }
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Failed to Create New Regional Dept',
                color: 'red',
                icon: 'warning'
              })
              this.iconregionaldept = false
            }
          })
          .catch(error => {
            this.$q.loading.hide()
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            })
            this.iconregionaldept = false
            this.onResetRegionalDept()
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      },
      onResetRegionalDept () {
        this.regionaldept = null
      },
      doUpdateRegional (id) {
        api.get('/master/getregionallistbyid', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id' : id
          }
        })
        .then(response => {
          var data = response.data
          this.upd.regionallistid = data.regionallistid
          this.upd.regional = data.regional
          this.upd.division = data.division
          this.iconview = true
        })
        .catch(error => {
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      doDeleteRegional (id) {
        api.get('/master/getregionallistbyid', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id' : id
          }
        })
        .then(response => {
          var data = response.data
          this.del.delid = data.regionallistid
          this.del.delname = data.regional.regionaldept + ' - ' + data.division.divisioname
          this.icondelete = true
        })
        .catch(error => {
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      deleteRegionalList(id) {
        this.$q.loading.show()
        api.delete('/master/deleteregionallist',{
          headers:{
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id' : id
          }
        })
        .then(response => {
          if (response.data == 1) {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Record Deleted',
              color: 'secondary',
              icon: 'check_circle'
            })
            this.icondelete = false
            this.getRegionalList()
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Delete Record',
              color: 'red',
              icon: 'warning'
            })
            this.icondelete = false
            this.getRegionalList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      updateRegionalList(){
        this.$q.loading.show()
        if (this.upd.regional != null && this.upd.division != null){
          var body = {
            'regionalistid' : this.upd.regionallistid,
            'regionalid' : this.upd.regional.regionalid,
            'divisionid' : this.upd.division.divisionid
          }
          api.patch('/master/updateregionallist', body, {
            headers: {
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var data = response.data
            if (data.datecreated == null) {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Record Registered. Cannot Save Duplicate Data',
                color: 'red',
                icon: 'warning'
              })
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Record Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconview = false
              this.getRegionalList()
            }
          })
          .catch(error => {
            this.$q.loading.hide()
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            })
            this.iconview = false
            this.getRegionalList()
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      }
    },
    beforeMount () {
      this.getRegionalList()
      this.getDivisionOptions()
      this.getRegionalOptions()
    }
  }