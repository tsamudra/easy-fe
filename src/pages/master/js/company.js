const columnscompany = [
  {
    name: 'no',
    required: true,
    label: 'No',
    align: 'center',
    sortable: true
  },
  { name: 'companyname', align: 'center', label: 'Company Name', field: 'companyname', sortable: true },
  { name: 'companyid', label: 'Action', field: 'companyid', align: 'center' }
]

import {ref} from 'vue'
import {api} from 'boot/axios'
import { useQuasar } from 'quasar'

export default {  
  setup () {
    const $q = useQuasar()
  },
  data () {
    return {
      filtercompany: ref(''),
      iconcompany: ref(false),
      icondelete: ref(false),
      iconview: ref(false),
      company: ref(''),
      columnscompany,
      rowscompany: [],
      cp: {
        companyid: ref(''),
        companyname: ref('')
      }
    }
  },
  methods: {
    getCompanyList(){
      api.get('/master/getcompany', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        var data = response.data
        var i = 0
        for (var key in data) {
          data[i]['no'] = i + 1
          i++
        }
        this.rowscompany = data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    onSubmitCompany () {
      this.$q.loading.show()
      if (this.company != ''){
        var body = {
          'companyid': null,
          'companyname': this.company
        }
        api.post('/master/createcompany', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          console.log(data)
          if (data.companyname != null){
            if (data.datecreated != null ){
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Company Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconcompany = false
              this.getCompanyList()
              this.onResetCompany()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.companyname,
                color: 'red',
                icon: 'warning'
              })
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Create Company',
              color: 'red',
              icon: 'warning'
            })
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message : 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onResetCompany () {
      this.company = null
    },
    doUpdateCompany (id) {
      api.get('/master/getcompanybyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        var data = response.data
        this.cp.companyid = data.companyid
        this.cp.companyname = data.companyname
        this.iconview = true
      })
      .catch(error => {
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    doDeleteCompany (id) {
      api.get('/master/getcompanybyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        var data = response.data
        this.cp.companyid = data.companyid
        this.cp.companyname = data.companyname
        this.icondelete = true
      })
      .catch(error => {
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    deleteCompany(id){
      this.$q.loading.show()
      api.delete('master/deletecompany',{
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id':id
        }
      })
      .then(response => {
        if (response.data == 1){
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Company Deleted',
            color: 'secondary',
            icon: 'check_circle'
          })
          this.icondelete = false
          this.getCompanyList()
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Failed to Delete Company',
            color: 'red',
            icon: 'warning'
          })
          this.icondelete = false
          this.getCompanyList()
        }
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    updateCompany(){
      this.$q.loading.show()
      if (this.cp.companyname != ''){
        var body = {
          'companyid' : this.cp.companyid,
          'companyname' : this.cp.companyname
        }
        api.patch('/master/updatecompany', body, {
          headers:{
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if (data.companyname != null){
            if (data.datecreated != null){
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Company Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconview = false
              this.getCompanyList()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.companyname,
                color: 'red',
                icon: 'warning'
              })
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Update Company',
              color: 'red',
              icon: 'warning'
            })
            this.iconview = false
            this.getCompanyList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.iconview = false
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    }
  },
  beforeMount () {
    this.getCompanyList()
  }
  }