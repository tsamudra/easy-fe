const productivityColumns = [
  { name: 'regionalname', align: 'center', label: 'REGIONAL NAME', field: 'regionalname', sortable: true, align: 'center' },
  { name: 'team', label: 'TEAM', field: 'team', sortable: true, align: 'center' },
  { name: 'attedance', label: '% ATTEDANCE', field: 'attedance', align: 'center' },
  { name: 'ticketpoint', label: '% TICKET POINT', field: 'ticketpoint', align: 'center' },
  { name: 'value', label: 'VALUE', field: 'value', align: 'center' }
]

const productivityRows = [
  {
    regionalname: 1,
    team: 159,
    attedance: 6.0,
    countofln: 24,
    ticketpoint: 4.0,
    value: 87
  },
  {
    regionalname: 2,
    team: 237,
    attedance: 9.0,
    countofln: 37,
    ticketpoint: 4.3,
    value: 129  
},
  {
    regionalname: 3,
    team: 262,
    attedance: 16.0,
    countofln: 23,
    ticketpoint: 6.0,
    value: 337  
  },
  {
    regionalname: 4,
    team: 305,
    attedance: 3.7,
    countofln: 67,
    ticketpoint: 4.3,
    value: 413  
  },
  {
    regionalname: 5,
    team: 356,
    attedance: 16.0,
    countofln: 49,
    ticketpoint: 3.9,
    value: 327  
  },
  {
    regionalname: 6,
    team: 375,
    attedance: 0.0,
    countofln: 94,
    ticketpoint: 0.0,
    value: 50  
},
  {
    regionalname: 7,
    team: 392,
    attedance: 0.2,
    countofln: 98,
    ticketpoint: 0,
    value: 38  
  },
  {
    regionalname: 8,
    team: 408,
    attedance: 3.2,
    countofln: 87,
    ticketpoint: 6.5,
    value: 562 
  },
  {
    regionalname: 9,
    team: 452,
    attedance: 25.0,
    countofln: 51,
    ticketpoint: 4.9,
    value: 326  
  },
  {
    regionalname: 10,
    team: 518,
    attedance: 26.0,
    countofln: 65,
    ticketpoint: 7,
    value: 54  
  }
]

const attedanceColumns = [
  { name: 'regionalname', align: 'center', label: 'REGIONAL NAME', field: 'regionalname', sortable: true, align: 'center' },
  { name: 'team', label: 'TEAM', field: 'team', sortable: true, align: 'center' },
  { name: 'totalschedule', label: 'TOTAL SCHEDULE', field: 'totalschedule', align: 'center' },
  { name: 'countofln', label: 'COUNT OF LN', field: 'countofln', align: 'center' },
  { name: 'countoflate', label: 'COUNT OF LATE', field: 'countoflate', align: 'center' },
  { name: 'countofoverdue', label: 'COUNT OF OVERDUE', field: 'countofoverdue', align: 'center' },
  { name: 'attedancepoint', label: 'ATTEDANCE POINT', field: 'attedancepoint', align: 'center' },
  { name: 'value', label: 'VALUE', field: 'value', align: 'center' }
]

const attedanceRows = [
  {
    regionalname: 1,
    team: 159,
    totalschedule: 6.0,
    countofln: 24,
    countoflate: 4.0,
    countofoverdue: 87,
    attedancepoint: '14%',
    value: 66
  },
  {
    regionalname: 2,
    team: 237,
    totalschedule: 9.0,
    countofln: 37,
    countoflate: 4.3,
    countofoverdue: 129,
    attedancepoint: '8%',
    value: 66
},
  {
    regionalname: 3,
    team: 262,
    totalschedule: 16.0,
    countofln: 23,
    countoflate: 6.0,
    countofoverdue: 337,
    attedancepoint: '6%',
    value: 66  
  },
  {
    regionalname: 4,
    team: 305,
    totalschedule: 3.7,
    countofln: 67,
    countoflate: 4.3,
    countofoverdue: 413,
    attedancepoint: '3%',
    value: 66  
  },
  {
    regionalname: 5,
    team: 356,
    totalschedule: 16.0,
    countofln: 49,
    countoflate: 3.9,
    countofoverdue: 327,
    attedancepoint: '7%',
    value: 66  
  },
  {
    regionalname: 6,
    team: 375,
    totalschedule: 0.0,
    countofln: 94,
    countoflate: 0.0,
    countofoverdue: 50,
    attedancepoint: '0%',
    value: 66  
},
  {
    regionalname: 7,
    team: 392,
    totalschedule: 0.2,
    countofln: 98,
    countoflate: 0,
    countofoverdue: 38,
    attedancepoint: '0%',
    value: 66  
  },
  {
    regionalname: 8,
    team: 408,
    totalschedule: 3.2,
    countofln: 87,
    countoflate: 6.5,
    countofoverdue: 562,
    attedancepoint: '0%',
    value: 66 
  },
  {
    regionalname: 9,
    team: 452,
    totalschedule: 25.0,
    countofln: 51,
    countoflate: 4.9,
    countofoverdue: 326,
    attedancepoint: '2%',
    value: 66  
  },
  {
    regionalname: 10,
    team: 518,
    totalschedule: 26.0,
    countofln: 65,
    countoflate: 7,
    countofoverdue: 54,
    attedancepoint: '12%',
    value: 66
  }
]

const ticketPointColumns = [
  { name: 'regionalname', align: 'center', label: 'REGIONAL NAME', field: 'regionalname', sortable: true, align: 'center' },
  { name: 'team', label: 'TEAM', field: 'team', sortable: true, align: 'center' },
  { name: 'a', label: 'A', field: 'a', align: 'center' },
  { name: 'a1', label: 'A1', field: 'a1', align: 'center' },
  { name: 'a2', label: 'A2', field: 'a2', align: 'center' },
  { name: 'a3', label: 'A3', field: 'a3', align: 'center' },
  { name: 'b', label: 'B', field: 'b', align: 'center' },
  { name: 'b1', label: 'B1', field: 'b1', align: 'center' },
  { name: 'b2', label: 'B2', field: 'b2', align: 'center' },
  { name: 'b3', label: 'B3', field: 'b3', align: 'center' },
  { name: 'req', label: 'REQ', field: 'req', align: 'center' },
  { name: 'totalticketpoint', label: 'TOTAL TICKET POINT', field: 'totalticketpoint', align: 'center' }
]

const ticketPointRows = [
  {
    regionalname: 1,
    team: 159,
    a: 6.0,
    a1: 24,
    a2: 4.0,
    a3: 87,
    b: '14%',
    b1: 66,
    b2: 23,
    b3: 1,
    req: '1%',
    totalticketpoint: 2
  },
  {
    regionalname: 2,
    team: 237,
    a: 9.0,
    a1: 37,
    a2: 4.3,
    a3: 129,
    b: '8%',
    b1: 66,
    b2: 21,
    b3: 1,
    req: '1%',
    totalticketpoint: 2  
},
  {
    regionalname: 3,
    team: 262,
    a: 16.0,
    a1: 23,
    a2: 6.0,
    a3: 337,
    b: '6%',
    b1: 66,
    b2: 21,
    b3: 1,
    req: '1%',
    totalticketpoint: 2  
  },
  {
    regionalname: 4,
    team: 305,
    a: 3.7,
    a1: 67,
    a2: 4.3,
    a3: 413,
    b: '3%',
    b1: 66,
    b2: 21,
    b3: 1,
    req: '1%',
    totalticketpoint: 2  
  },
  {
    regionalname: 5,
    team: 356,
    a: 16.0,
    a1: 49,
    a2: 3.9,
    a3: 327,
    b: '7%',
    b1: 66,
    b2: 21,
    b3: 1,
    req: '1%',
    totalticketpoint: 2  
  },
  {
    regionalname: 6,
    team: 375,
    a: 0.0,
    a1: 94,
    a2: 0.0,
    a3: 50,
    b: '0%',
    b1: 66,
    b2: 21,
    b3: 1,
    req: '1%',
    totalticketpoint: 2  
},
  {
    regionalname: 7,
    team: 392,
    a: 0.2,
    a1: 98,
    a2: 0,
    a3: 38,
    b: '0%',
    b1: 66,
    b2: 21,
    b3: 1,
    req: '1%',
    totalticketpoint: 2  
  },
  {
    regionalname: 8,
    team: 408,
    a: 3.2,
    a1: 87,
    a2: 6.5,
    a3: 562,
    b: '0%',
    b1: 66,
    b2: 21,
    b3: 1,
    req: '1%',
    totalticketpoint: 2 
  },
  {
    regionalname: 9,
    team: 452,
    a: 25.0,
    a1: 51,
    a2: 4.9,
    a3: 326,
    b: '2%',
    b1: 66,
    b2: 21,
    b3: 1,
    req: '1%',
    totalticketpoint: 2  
  },
  {
    regionalname: 10,
    team: 518,
    a: 26.0,
    a1: 65,
    a2: 7,
    a3: 54,
    b: '12%',
    b1: 66,
    b2: 21,
    b3: 1,
    req: '1%',
    totalticketpoint: 2  
  }
]

import { ref } from 'vue'

export default {
    data () {
      return {
        productivityColumns,
        productivityRows,
        attedanceColumns,
        attedanceRows,
        ticketPointColumns,
        ticketPointRows
      }
    },
    methods: {
    },
    beforeMount () {
    }
  }