const columns = [
  {
    name: 'no',
    required: true,
    label: 'NO',
    align: 'center',
    field: row => row.name,
    format: val => `${val}`,
    sortable: true
  },
  { name: 'callsign', align: 'center', label: 'CALLSIGN', field: 'callsign', sortable: true, align: 'center' },
  { name: 'fullname', label: 'EMPLOYEE NAME', field: 'fullname', sortable: true, align: 'center' },
  { name: 'regional', label: 'REGIONAL', field: 'regional', align: 'center' },
  { name: 'area', label: 'AREA', field: 'area', align: 'center' },
  { name: 'mainjob', label: 'MAIN JOB', field: 'mainjob', sortable: true, align: 'center' },
  { name: 'position', label: 'POSITION', field: 'position', sortable: true, align: 'center' }
]

const rows = [
  {
    no: 1,
    callsign: 159,
    fullname: 6.0,
    area: 4.0,
    regional: 87,
    mainjob: '14%',
    position: '1%'
  },
  {
    no: 2,
    callsign: 237,
    fullname: 9.0,
    lastlogin: 37,
    area: 4.3,
    regional: 129,
    mainjob: '8%',
    position: '1%'
  },
  {
    no: 3,
    callsign: 262,
    fullname: 16.0,
    lastlogin: 23,
    area: 6.0,
    regional: 337,
    mainjob: '6%',
    position: '7%'
  },
  {
    no: 4,
    callsign: 305,
    fullname: 3.7,
    lastlogin: 67,
    area: 4.3,
    regional: 413,
    mainjob: '3%',
    position: '8%'
  },
  {
    no: 5,
    callsign: 356,
    fullname: 16.0,
    lastlogin: 49,
    area: 3.9,
    regional: 327,
    mainjob: '7%',
    position: '16%'
  },
  {
    no: 6,
    callsign: 375,
    fullname: 0.0,
    lastlogin: 94,
    area: 0.0,
    regional: 50,
    mainjob: '0%',
    position: '0%'
  },
  {
    no: 7,
    callsign: 392,
    fullname: 0.2,
    lastlogin: 98,
    area: 0,
    regional: 38,
    mainjob: '0%',
    position: '2%'
  },
  {
    no: 8,
    callsign: 408,
    fullname: 3.2,
    lastlogin: 87,
    area: 6.5,
    regional: 562,
    mainjob: '0%',
    position: '45%'
  },
  {
    no: 9,
    callsign: 452,
    fullname: 25.0,
    lastlogin: 51,
    area: 4.9,
    regional: 326,
    mainjob: '2%',
    position: '22%'
  },
  {
    no: 10,
    callsign: 518,
    fullname: 26.0,
    lastlogin: 65,
    area: 7,
    regional: 54,
    mainjob: '12%',
    position: '6%'
  }
]

import { ref } from 'vue'

export default {
    data () {
      return {
        select: ref(''),
        filter: ref(''),
        columns,
        rows,
        options : [
          'Job A',
          'Job B',
          'Job C'
        ]
      }
    },
    methods: {
    },
    beforeMount () {
    }
  }