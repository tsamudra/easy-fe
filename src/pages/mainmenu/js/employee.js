const columns = [
  {
    name: '',
    required: true,
    label: '',
    align: 'center'
  },
  {
    name: 'no',
    required: true,
    label: 'No',
    align: 'center',
    field: row => row.name,
    format: val => `${val}`,
    sortable: true
  },
  { name: 'employeeid', align: 'center', label: 'Employee ID', field: 'employeeid', sortable: true, align: 'center' },
  { name: 'fullname', align: 'center', label: 'Employee Name', field: 'fullname', sortable: true, align: 'center' },
  { name: 'callsign', label: 'Callsign', field: 'callsign', sortable: true, align: 'center' },
  { name: 'nikktp', label: 'NIK KTP', field: 'nikktp', align: 'center' },
  { name: 'nik', label: 'NIK', field: 'nik', align: 'center' },
  { name: 'company', label: 'Company Name', field: 'company', align: 'center' },
  { name: 'regional', label: 'Regional', field: 'regional', sortable: true, align: 'center' },
  { name: 'division', label: 'Division', field: 'division', sortable: true, align: 'center' },
  { name: 'mainjob', label: 'Main Job', field: 'mainjob', sortable: true, align: 'center' },
  { name: 'position', label: 'Position', field: 'position', sortable: true, align: 'center' },
  { name: 'level', label: 'Level', field: 'level', sortable: true, align: 'center' },
  { name: 'email', label: 'Email', field: 'email', sortable: true, align: 'center' },
  { name: 'address', label: 'Address', field: 'address', sortable: true, align: 'center' },
  { name: 'education', label: 'Education', field: 'education', sortable: true, align: 'center' },
  { name: 'phonenum', label: 'Phone Number', field: 'phonenum', sortable: true, align: 'center' },
  { name: 'remark', label: 'Remark', field: 'remark', sortable: true, align: 'center' },
  { name: 'joindate', label: 'Join Date', field: 'joindate', sortable: true, align: 'center' },
  { name: 'joincallsign', label: 'Join Callsign', field: 'joincallsign', sortable: true, align: 'center' },
  { name: 'action', label: 'Action', field: 'action', sortable: true, align: 'center' }
]

import { ref } from 'vue'
import {api} from 'boot/axios'
import {useQuasar} from 'quasar'
export default {  
  setup () {
    const $q = useQuasar();
  },
  data () {
    return {
      role: localStorage.getItem('user-role'),
      checkAll: ref(false),
      file: null,
      filter: ref(''),
      iconmultiple: ref(false),
      selectCompany: null,
      selectRegional: null,
      selectDivision: null,
      selectPosition: null,
      columns,
      rows : [],
      optionsCompany: [],
      optionsRegional: [],
      optionsDivision: [],
      optionsPosition: [],
      optionsMainjob: [],
      optionsLevel: [],
      listCheckedEmployee: [],
      confDel: {
        empId: ref(''),
        empName: ref('')
      },
      icondelete: ref(false)
    }
  },
  methods: {
    getEmployees(select) {
      if (this.selectCompany != null){
        var company = this.selectCompany.companyname
      } else {
        var company = ''
      }

      if (this.selectRegional != null){
        var regional = this.selectRegional.regionaldept
      } else {
        var regional = ''
      }

      if (this.selectDivision != null){
        var division = this.selectDivision.divisioname
      } else {
        var division = ''
      }

      if (this.selectPosition != null){
        var position = this.selectPosition.positionname
      } else {
        var position = ''
      }

      api.get('/employee/getallactiveemployeewithfilter', {
        headers: {
          Authorization : localStorage.getItem('user-token')
        },
        params: {
          'company': company,
          'regional': regional,
          'division': division,
          'position': position
        }
      })
      .then(response => {
        var datas = response.data
        var i = 0;
        for (var key in datas) {
          datas[i]['no'] = i + 1
          if (select == false){
            datas[i]['selected'] = false
          } else {
            datas[i]['selected'] = true
            this.listCheckedEmployee.push(datas[i].employeeid)
          }
          i++
        }
        this.rows = datas
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getCompanyList(){
      api.get('/master/getcompany',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsCompany = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getDivisionList(){
      api.get('/master/getalldivision',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsDivision = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getRegionalList(){
      api.get('/master/getallregional',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsRegional = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getPositionList(){
      api.get('/master/getallposition',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsPosition = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    filterSearch () {
      this.getEmployees(false)
    },
    resetFilter () {
      this.selectCompany = null
      this.selectRegional = null
      this.selectDivision = null
      this.selectPosition = null
      this.getEmployees(false)
    },
    onResetSingle() {
      this.callsign = null
      this.nik = null
      this.fullname = null
      this.company = null
      this.regional = null
      this.area = null
      this.mainjob = null
      this.position = null
      this.joindate = null
      this.address = null
      this.pendidikan = null
      this.phonenum = null
    },
    onSubmitBulk () {
      this.$q.loading.show()
      if(this.file != null){
        var filename = this.file.name
        var arrayFile = filename.split(".")
        if(arrayFile[1] == 'xlsx'){
          var formData = new FormData()
          formData.append('username', localStorage.getItem('user-username'))
          formData.append('file', this.file)
          console.log(formData)
          api.post('/master/uploadtemplate', formData,  {
            headers: {
              Authorization: localStorage.getItem('user-token'),
              'Content-Type': 'multipart/form-data;charset=UTF-8'
            }
          })
          .then(response => {
            var data = response.data
            if (data.status == "OK"){
              this.$q.loading.hide()
              this.file = null
              this.iconmultiple = false
              this.getEmployees(false)
              this.$q.notify({
                message: data.message,
                color: 'secondary',
                icon: 'check_circle'
              },5000)
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.message,
                color: 'red',
                icon: 'warning'
              },2000)
            }
          })
          .catch(error => {
            this.$q.loading.hide()
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            },2000)
            console.log(error)
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Excel File Only (xlsx)',
            color: 'red',
            icon: 'warning'
          })
          this.file = null
        }
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onResetBulk () {
      this.file = null
    },
    editEmployee(id) {
      this.$router.push('/extended/editemployee/'+id+'/action/details')
    },
    promoteEmployee(id){
      this.$router.push('/extended/editemployee/'+id+'/action/promote')
    },
    movementEmployee(id){
      this.$router.push('/extended/editemployee/'+id+'/action/movement')
    },
    resignEmployee(id){
      this.$router.push('/extended/resignemployee/'+id)
    },
    showBeforeDelete(id){
      this.$q.loading.show()
      api.get('/employee/getemployeebyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params:{
          'id' : id
        }
      })
      .then(response => {
        this.$q.loading.hide()
        var data = response.data
        this.confDel.empId = data.employeeid
        this.confDel.empName = data.fullname
        this.icondelete = true
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    deleteEmployee(id){
      this.$q.loading.show()
      api.get('/employee/softdeleteemployee',{
        headers:{
          Authorization: localStorage.getItem('user-token')
        },
        params:{
          'id': id,
          'username' : localStorage.getItem('user-username')
        }
      })
      .then(response => {
        var data = response.data
        if (data.employeeid != null) {
          this.$q.loading.hide()
          this.$q.notify({
            message: data.fullname + ' Moved to Bin',
            color: 'secondary',
            icon: 'check_circle'
          })
          this.icondelete = false
          this.getEmployees(false)
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Failed to Delete',
            color: 'red',
            icon: 'warning'
          })
          this.icondelete = false
        }
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    downloadTemplate(){
      this.$q.loading.show()
      var body = {
        'username' : localStorage.getItem('user-username'),
        'ids' : this.listCheckedEmployee
      }
      api.post('/master/gettemplate', body, {
        responseType: 'blob',
        headers:{
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        var fileName = 'Employee Template.xlsx'
        link.setAttribute('download', fileName)
        document.body.appendChild(link)
        link.click()
        this.$q.loading.hide()
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    listOfEmployees(check, id){
      if (check == true) {
        this.listCheckedEmployee.push(id)
      } else {

        var index = this.listCheckedEmployee.indexOf(id);
        if (index > -1) {
          this.listCheckedEmployee.splice(index, 1);
        }
      }
      console.log(this.listCheckedEmployee)
    },
    downloadReport(){
      this.$q.loading.show()
      if (this.selectCompany != null){
        var company = this.selectCompany.companyname
      } else {
        var company = ''
      }

      if (this.selectRegional != null){
        var regional = this.selectRegional.regionaldept
      } else {
        var regional = ''
      }

      if (this.selectDivision != null){
        var division = this.selectDivision.divisioname
      } else {
        var division = ''
      }

      if (this.selectPosition != null){
        var position = this.selectPosition.positionname
      } else {
        var position = ''
      }
      var body = {
        'company': company,
        'regional': regional,
        'division': division,
        'position': position,
        'startdate': null,
        'enddate' : null
      }
      api.post('/master/getemployeereport', body, {
        responseType: 'blob',
        headers:{
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        var fileName = 'Employee Report.xlsx'
        link.setAttribute('download', fileName)
        document.body.appendChild(link)
        link.click()
        this.$q.loading.hide()
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    }
  },
  beforeMount () {
    this.getEmployees(false)
    this.getCompanyList()
    this.getDivisionList()
    this.getPositionList()
    this.getRegionalList()
  }
}