const columns = [
  {
    name: '',
    required: true,
    label: '',
    align: 'center'
  },
  {
    name: 'no',
    required: true,
    label: 'No',
    align: 'center',
    field: row => row.name,
    format: val => `${val}`,
    sortable: true
  },
  { name: 'employeeid', align: 'center', label: 'Employee ID', field: 'employeeid', sortable: true, align: 'center' },
  { name: 'callsign', label: 'Callsign', field: 'callsign', sortable: true, align: 'center' },
  { name: 'nikktp', label: 'NIK KTP', field: 'nikktp', align: 'center' },
  { name: 'nik', label: 'NIK', field: 'nik', align: 'center' },
  { name: 'company', label: 'Company Name', field: 'company', align: 'center' },
  { name: 'regional', label: 'Regional', field: 'regional', sortable: true, align: 'center' },
  { name: 'division', label: 'Division', field: 'division', sortable: true, align: 'center' },
  { name: 'mainjob', label: 'Main Job', field: 'mainjob', sortable: true, align: 'center' },
  { name: 'position', label: 'Position', field: 'position', sortable: true, align: 'center' },
  { name: 'level', label: 'Level', field: 'level', sortable: true, align: 'center' },
  { name: 'email', label: 'Email', field: 'email', sortable: true, align: 'center' },
  { name: 'address', label: 'Address', field: 'address', sortable: true, align: 'center' },
  { name: 'education', label: 'Education', field: 'education', sortable: true, align: 'center' },
  { name: 'phonenum', label: 'Phone Number', field: 'phonenum', sortable: true, align: 'center' },
  { name: 'remark', label: 'Remark', field: 'remark', sortable: true, align: 'center' },
  { name: 'joindate', label: 'Join Date', field: 'joindate', sortable: true, align: 'center' },
  { name: 'employeeid', label: 'Action', field: 'employeeid', sortable: true, align: 'center' }
]

import { api } from 'src/boot/axios'
import { ref } from 'vue'

export default {
  
    data () {
      return {
        iconview : ref(false),
        checkAll: ref(false),
        select: ref(''),
        filter: ref(''),
        startdate: ref(''),
        enddate: ref(''),
        columns,
        rows : [],
        listCheckedEmployee: [],
        detail: {
          employeeid: ref(''),
          callsign: ref(''),
          nik: ref(''),
          nik_ktp: ref(''),
          fullname: ref(''),
          company: ref(''),
          regional: ref(''),
          division: ref(''),
          mainjob: ref(''),
          position: ref(''),
          joindate: ref(''),
          joincallsign: ref(''),
          releasecallsign: ref(''),
          address: ref(''),
          education: ref(''),
          level: ref(''),
          phonenum: ref(''),
          email: ref(''),
          remark: null,
        },
      }
    },
    methods: {
      getEmployees(select){
        var start = ''
        var end = ''
        if (this.startdate != ''){
          start = this.formatDate(this.startdate)
        }
        if (this.enddate != ''){
          end = this.formatDate(this.enddate)
        }
        api.get('/employee/getalloffemployeewithfilter', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'startdate' : start,
            'enddate': end
          }
        })
        .then(response => {
          var datas = response.data
          var i = 0
          for (var key in datas) {
            datas[i]['no'] = i + 1
            if (select == false){
              datas[i]['selected'] = false
            } else {
              datas[i]['selected'] = true
              this.listCheckedEmployee.push(datas[i].employeeid)
            }
            i++
          }
          this.rows = datas
        })
        .catch(error => {
          console.log('error : ' + error)
        })
      },
      filterSearch() {
        this.$q.loading.show()
        if (this.startdate != '' && this.enddate != '') {
          this.$q.loading.hide()
          this.getEmployees(false)
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Date Periode',
            color: 'red',
            icon: 'warning'
          })
        }
      },
      resetFilter() {
        this.startdate = ref('')
        this.enddate = ref('')
        this.getEmployees(false)
      },
      doView(id){
        api.get('/employee/getempbyid', {
          headers: {
              Authorization : localStorage.getItem('user-token')
          },
          params : {
              'id' : id
          }
        })
        .then(response => {
            if (response.data != '') {
                this.detailsData(response.data)
                this.iconview = true
            } else {
                this.$q.notify({
                    message: 'Employee Data Not Found',
                    color: 'red',
                    icon: 'warning'
                })
                this.getEmployees()
            }
        })
        .catch(error => {
            console.log('message : ' + error)
            this.$q.notify({
                message: error,
                color: 'red',
                icon: 'warning'
            })
            this.getEmployees()
        })
      },
      doDelete(id) {
        console.log(id)
      },
      detailsData(data){
        this.detail.callsign = data.callsign
        this.detail.nik_ktp = data.nikktp
        this.detail.nik = data.nik
        this.detail.fullname = data.fullname
        this.detail.education = data.education
        this.detail.address = data.address
        this.detail.joindate = data.joindate
        this.detail.joincallsign = data.joincallsign
        this.detail.phonenum = data.phonenum
        this.detail.regional = data.regional
        this.detail.division = data.division
        this.detail.mainjob = data.mainjob
        this.detail.position = data.position
        this.detail.level = data.level
        this.detail.joindate = data.joindate
        this.detail.company = data.company
        this.detail.email = data.email
        this.detail.remark = data.remark
      },
      listOfEmployees(check, id){
        if (check == true) {
          this.listCheckedEmployee.push(id)
        } else {
  
          var index = this.listCheckedEmployee.indexOf(id);
          if (index > -1) {
            this.listCheckedEmployee.splice(index, 1);
          }
        }
        console.log(this.listCheckedEmployee)
      },
      doRestore(){
        this.$q.loading.show()  
        if (this.listCheckedEmployee[0] != null) {
          var body = {
            'ids' : this.listCheckedEmployee,
            'username' : localStorage.getItem('user-username')
          }
          api.patch('/employee/bulkrestoreemployee',body, {
            headers: {
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            this.$q.loading.hide()
            this.$q.notify({
              message: response.data,
              color: 'secondary',
              icon: 'check_circle'
            })
            this.getEmployees(false)
          })
          .catch(error => {
            this.$q.loading.hide()
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            })
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Select at Least one Data',
            color: 'red',
            icon: 'warning'
          })
          console.log(error)
        }
      },
      doPermanent(){
        if (this.listCheckedEmployee[0] != null) {
          var body = {
            'ids' : this.listCheckedEmployee,
            'username' : localStorage.getItem('user-username')
          }
          api.patch('/employee/bulkdeleteemployee',body, {
            headers: {
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            this.$q.loading.hide()
            this.$q.notify({
              message: response.data,
              color: 'secondary',
              icon: 'check_circle'
            })
            this.getEmployees(false)
          })
          .catch(error => {
            this.$q.loading.hide()
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            })
          })
        } else {
          this.$q.notify({
            message: 'Please Select at Least one Data',
            color: 'red',
            icon: 'warning'
          })
        }
      },
      formatDate (date) {
        var formatDate = date.replace(/\//g, '-')
        var datearray = formatDate.split('-')
        var newdate = datearray[2] + '-' + datearray[1] + '-' + datearray[0]
        return newdate
      },
    },
    beforeMount () {
      this.getEmployees(false)
    }
  }