const columns = [
  {
    name: 'no',
    required: true,
    label: 'NO',
    align: 'center',
    field: row => row.name,
    format: val => `${val}`,
    sortable: true
  },
  { name: 'name', align: 'center', label: 'NAME', field: 'name', sortable: true, align: 'center' },
  { name: 'codename', label: 'CODE NAME', field: 'codename', sortable: true, align: 'center' },
  { name: 'hubcode', label: 'HUB CODE', field: 'hubcode', align: 'center' },
  { name: 'jobtitle', label: 'Skill Set', field: 'jobtitle', align: 'center' },
  { name: 'skillset', label: 'SKILL SET', field: 'skillset', align: 'center' },
  { name: 'numberplate', label: 'NUMBER PLATE', field: 'numberplate', sortable: true, align: 'center' },
  { name: 'company', label: 'COMPANY', field: 'company', sortable: true, align: 'center' },
  { name: 'action', label: 'ACTION', field: 'action', sortable: true, align: 'center' }
]

const rows = [
  {
    no: 1,
    name: 159,
    codename: 6.0,
    hubcode: 24,
    jobtitle: 4.0,
    skillset: 87,
    numberplate: '14%',
    company: 8,
    action: '1%'
  },
  {
    no: 2,
    name: 237,
    codename: 9.0,
    hubcode: 37,
    jobtitle: 4.3,
    skillset: 129,
    numberplate: '8%',
    company: 8,
    action: '1%'
  },
  {
    no: 3,
    name: 262,
    codename: 16.0,
    hubcode: 23,
    jobtitle: 6.0,
    skillset: 337,
    numberplate: '6%',
    company: 8,
    action: '7%'
  },
  {
    no: 4,
    name: 305,
    codename: 3.7,
    hubcode: 67,
    jobtitle: 4.3,
    skillset: 413,
    numberplate: '3%',
    company: 8,
    action: '8%'
  },
  {
    no: 5,
    name: 356,
    codename: 16.0,
    hubcode: 49,
    jobtitle: 3.9,
    skillset: 327,
    numberplate: '7%',
    company: 8,
    action: '16%'
  },
  {
    no: 6,
    name: 375,
    codename: 0.0,
    hubcode: 94,
    jobtitle: 0.0,
    skillset: 50,
    numberplate: '0%',
    company: 8,
    action: '0%'
  },
  {
    no: 7,
    name: 392,
    codename: 0.2,
    hubcode: 98,
    jobtitle: 0,
    skillset: 38,
    numberplate: '0%',
    company: 8,
    action: '2%'
  },
  {
    no: 8,
    name: 408,
    codename: 3.2,
    hubcode: 87,
    jobtitle: 6.5,
    skillset: 562,
    numberplate: '0%',
    company: 8,
    action: '45%'
  },
  {
    no: 9,
    name: 452,
    codename: 25.0,
    hubcode: 51,
    jobtitle: 4.9,
    skillset: 326,
    numberplate: '2%',
    company: 8,
    action: '22%'
  },
  {
    no: 10,
    name: 518,
    codename: 26.0,
    hubcode: 65,
    jobtitle: 7,
    skillset: 54,
    numberplate: '12%',
    company: 8,
    action: '6%'
  }
]

import { ref } from 'vue'

export default {
    data () {
      return {
        icon: ref(false),
        select: ref(''),
        filter: ref(''),
        columns,
        rows,
        company: undefined,
        name: undefined,
        codename: undefined,
        area: undefined,
        jobtitle: undefined,
        skillset: undefined,
        numberplate: undefined,
        options : [
          'Company A',
          'Company B',
          'Company C'
        ],
        companyOptions : [
          'Company A',
          'Company B',
          'Company C'
        ],
        areaOptions: [
          'Area A',
          'Area B',
          'Area C'
        ],
        jobtitleOptions: [
          'Job Title A',
          'Job Title B',
          'Job Title C'
        ],
        skillsetOptions: [
          'Skill Set A',
          'Skill Set B',
          'Skill Set C'
        ]
      
      }
    },
    methods: {
      onSubmit () {
        if (accept.value !== true) {
          $q.notify({
            color: 'red-5',
            textColor: 'white',
            icon: 'warning',
            message: 'You need to accept the license and terms first'
          })
        }
        else {
          $q.notify({
            color: 'green-4',
            textColor: 'white',
            icon: 'cloud_done',
            message: 'Submitted'
          })
        }
      },
      onReset () {
        this.name = null
        this.codename = null
        this.password = null
        this.area = null
        this.jobtitle = null
        this.skillset = null
        this.numberplate = null
        this.company = null
      },
    },
    beforeMount () {
    }
  }